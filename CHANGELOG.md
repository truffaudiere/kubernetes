## [2.0.2](https://gitlab.com/to-be-continuous/kubernetes/compare/2.0.1...2.0.2) (2021-10-07)


### Bug Fixes

* use master or main for production env ([0822400](https://gitlab.com/to-be-continuous/kubernetes/commit/0822400ecd6c4965c3ba23d06b88c925497d7017))

## [2.0.1](https://gitlab.com/to-be-continuous/kubernetes/compare/2.0.0...2.0.1) (2021-09-16)


### Bug Fixes

* vault variant ([a484505](https://gitlab.com/to-be-continuous/kubernetes/commit/a484505184ab50ea958af488db06cb498c267dc1))

## [2.0.0](https://gitlab.com/to-be-continuous/kubernetes/compare/1.3.1...2.0.0) (2021-09-03)

### Features

* Change boolean variable behaviour ([3bd6a03](https://gitlab.com/to-be-continuous/kubernetes/commit/3bd6a03f63f3083d6982d37f3bada6cc9cd8c08c))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.3.1](https://gitlab.com/to-be-continuous/kubernetes/compare/1.3.0...1.3.1) (2021-07-08)

### Bug Fixes

* conflict between vault and scoped vars ([ed07480](https://gitlab.com/to-be-continuous/kubernetes/commit/ed074803fa0998586bd27d0c1c76eed35c069123))

## [1.3.0](https://gitlab.com/to-be-continuous/kubernetes/compare/1.2.0...1.3.0) (2021-06-19)

### Features

* support multi-lines environment variables substitution ([bc2f8d5](https://gitlab.com/to-be-continuous/kubernetes/commit/bc2f8d58ccdef31bd66afb845f8877007e91f026))

## [1.2.0](https://gitlab.com/to-be-continuous/kubernetes/compare/1.1.0...1.2.0) (2021-06-10)

### Features

* move group ([0042198](https://gitlab.com/to-be-continuous/kubernetes/commit/0042198c0f6e5c14877f5693b986dc778d9b5fef))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/kubernetes/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([f2c8894](https://gitlab.com/Orange-OpenSource/tbc/kubernetes/commit/f2c8894eb1f8d73fa7d37a93553a3f3b50f670f9))

## 1.0.0 (2021-05-06)

### Features

* initial release ([885caed](https://gitlab.com/Orange-OpenSource/tbc/kubernetes/commit/885caed8b3063252d72abbdb568f40a59e99585e))
