# =========================================================================================
# Copyright (C) 2021 Orange & contributors
#
# This program is free software; you can redistribute it and/or modify it under the terms 
# of the GNU Lesser General Public License as published by the Free Software Foundation; 
# either version 3 of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License along with this 
# program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth 
# Floor, Boston, MA  02110-1301, USA.
# =========================================================================================
variables:
  # Docker Image with Kubernetes CLI tool (can be overridden)
  K8S_KUBECTL_IMAGE: "bitnami/kubectl:latest"
  K8S_KUBE_SCORE_IMAGE: "zegl/kube-score:latest-helm"
  K8S_BASE_APP_NAME: "$CI_PROJECT_NAME"
  K8S_SCRIPTS_DIR: "."
  K8S_REVIEW_ENVIRONMENT_SCHEME: "https"
  # default production ref name (pattern)
  PROD_REF: '/^(master|main)$/'
  # default integration ref name (pattern)
  INTEG_REF: '/^develop$/'

stages:
  - test
  - deploy
  - production

.k8s-scripts: &k8s-scripts |
  # BEGSCRIPT
  set -xve

  function log_info() {
    echo -e "[\\e[1;94mINFO\\e[0m] $*"
  }

  function log_warn() {
    echo -e "[\\e[1;93mWARN\\e[0m] $*"
  }

  function log_error() {
    echo -e "[\\e[1;91mERROR\\e[0m] $*"
  }

  function fail() {
    log_error "$*"
    exit 1
  }

  function assert_defined() {
    if [[ -z "$1" ]]
    then
      log_error "$2"
      exit 1
    fi
  }

  function install_ca_certs() {
    certs=$1
    if [[ -z "$certs" ]]
    then
      return
    fi

    # import in system
    if echo "$certs" >> /etc/ssl/certs/ca-certificates.crt
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/certs/ca-certificates.crt\\e[0m"
    fi
    if echo "$certs" >> /etc/ssl/cert.pem
    then
      log_info "CA certificates imported in \\e[33;1m/etc/ssl/cert.pem\\e[0m"
    fi
  }

  function unscope_variables() {
    _scoped_vars=$(env | awk -F '=' "/^scoped__[a-zA-Z0-9_]+=/ {print \$1}" | sort)
    if [[ -z "$_scoped_vars" ]]; then return; fi
    log_info "Processing scoped variables..."
    for _scoped_var in $_scoped_vars
    do
      _fields=${_scoped_var//__/:}
      _condition=$(echo "$_fields" | cut -d: -f3)
      case "$_condition" in
      if) _not="";;
      ifnot) _not=1;;
      *)
        log_warn "... unrecognized condition \\e[1;91m$_condition\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
      ;;
      esac
      _target_var=$(echo "$_fields" | cut -d: -f2)
      _cond_var=$(echo "$_fields" | cut -d: -f4)
      _cond_val=$(eval echo "\$${_cond_var}")
      _test_op=$(echo "$_fields" | cut -d: -f5)
      case "$_test_op" in
      defined)
        if [[ -z "$_not" ]] && [[ -z "$_cond_val" ]]; then continue; 
        elif [[ "$_not" ]] && [[ "$_cond_val" ]]; then continue; 
        fi
        ;;
      equals|startswith|endswith|contains|in|equals_ic|startswith_ic|endswith_ic|contains_ic|in_ic)
        # comparison operator
        # sluggify actual value
        _cond_val=$(echo "$_cond_val" | tr '[:punct:]' '_')
        # retrieve comparison value
        _cmp_val_prefix="scoped__${_target_var}__${_condition}__${_cond_var}__${_test_op}__"
        _cmp_val=${_scoped_var#$_cmp_val_prefix}
        # manage 'ignore case'
        if [[ "$_test_op" == *_ic ]]
        then
          # lowercase everything
          _cond_val=$(echo "$_cond_val" | tr '[:upper:]' '[:lower:]')
          _cmp_val=$(echo "$_cmp_val" | tr '[:upper:]' '[:lower:]')
        fi
        case "$_test_op" in
        equals*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val" ]]; then continue; 
          fi
          ;;
        startswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != "$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == "$_cmp_val"* ]]; then continue; 
          fi
          ;;
        endswith*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val" ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val" ]]; then continue; 
          fi
          ;;
        contains*)
          if [[ -z "$_not" ]] && [[ "$_cond_val" != *"$_cmp_val"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "$_cond_val" == *"$_cmp_val"* ]]; then continue; 
          fi
          ;;
        in*)
          if [[ -z "$_not" ]] && [[ "__${_cmp_val}__" != *"__${_cond_val}__"* ]]; then continue; 
          elif [[ "$_not" ]] && [[ "__${_cmp_val}__" == *"__${_cond_val}__"* ]]; then continue; 
          fi
          ;;
        esac
        ;;
      *)
        log_warn "... unrecognized test operator \\e[1;91m${_test_op}\\e[0m in \\e[33;1m${_scoped_var}\\e[0m"
        continue
        ;;
      esac
      # matches
      _val=$(eval echo "\$${_target_var}")
      log_info "... apply \\e[32m${_target_var}\\e[0m from \\e[32m\$${_scoped_var}\\e[0m${_val:+ (\\e[33;1moverwrite\\e[0m)}"
      _val=$(eval echo "\$${_scoped_var}")
      export "${_target_var}"="${_val}"
    done
    log_info "... done"
  }

  # evaluate and export a secret
  # - $1: secret variable name
  function eval_secret() {
    name=$1
    value=$(eval echo "\$${name}")
    case "$value" in
    @b64@*)
      decoded=$(mktemp)
      errors=$(mktemp)
      if echo "$value" | cut -c6- | base64 -d > "${decoded}" 2> "${errors}"
      then
        # shellcheck disable=SC2086
        export ${name}="$(cat ${decoded})"
        log_info "Successfully decoded base64 secret \\e[33;1m${name}\\e[0m"
      else
        fail "Failed decoding base64 secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
      fi
      ;;
    @hex@*)
      decoded=$(mktemp)
      errors=$(mktemp)
      if echo "$value" | cut -c6- | sed 's/\([0-9A-F]\{2\}\)/\\\\x\1/gI' | xargs printf > "${decoded}" 2> "${errors}"
      then
        # shellcheck disable=SC2086
        export ${name}="$(cat ${decoded})"
        log_info "Successfully decoded hexadecimal secret \\e[33;1m${name}\\e[0m"
      else
        fail "Failed decoding hexadecimal secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
      fi
      ;;
    @url@*)
      url=$(echo "$value" | cut -c6-)
      if command -v curl > /dev/null
      then
        decoded=$(mktemp)
        errors=$(mktemp)
        if curl -s -S -f --connect-timeout 5 -o "${decoded}" "$url" 2> "${errors}"
        then
          # shellcheck disable=SC2086
          export ${name}="$(cat ${decoded})"
          log_info "Successfully curl'd secret \\e[33;1m${name}\\e[0m"
        else
          fail "Failed getting secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
        fi
      elif command -v wget > /dev/null
      then
        decoded=$(mktemp)
        errors=$(mktemp)
        if wget -T 5 -O "${decoded}" "$url" 2> "${errors}"
        then
          # shellcheck disable=SC2086
          export ${name}="$(cat ${decoded})"
          log_info "Successfully wget'd secret \\e[33;1m${name}\\e[0m"
        else
          fail "Failed getting secret \\e[33;1m${name}\\e[0m:\\n$(sed 's/^/... /g' "${errors}")"
        fi
      else
        fail "Couldn't get secret \\e[33;1m${name}\\e[0m: no http client found"
      fi
      ;;
    esac
  }

  function eval_all_secrets() {
    encoded_vars=$(env | grep -Ev '(^|.*_ENV_)scoped__' | awk -F '=' '/^[a-zA-Z0-9_]*=@(b64|hex|url)@/ {print $1}')
    for var in $encoded_vars
    do
      eval_secret "$var"
    done
  }
  
  # Converts a string to SCREAMING_SNAKE_CASE
  function to_ssc() {
    echo "$1" | tr '[:lower:]' '[:upper:]' | tr '[:punct:]' '_'
  }

  function awkenvsubst() {
    awk '{while(match($0,"[$]{[^}]*}")) {var=substr($0,RSTART+2,RLENGTH -3);val=ENVIRON[var];gsub(/["\\]/,"\\\\&", val);gsub("\n", "\\n", val);gsub("\r", "\\r", val);gsub("[$]{"var"}",val)}}1'
  }

  function login() {
    env=$1
    url=$2
    cacert=$3
    token=$4
    namespace=$5
    config=$6

    if [ -n "$config" ]; then
      log_info "--- \\e[32mlogin\\e[0m using kubeconfig content provided from env"
      export KUBECONFIG="$CI_PROJECT_DIR/.kubeconfig"
      echo "$config" > "$KUBECONFIG"
    elif [ -n "$KUBECONFIG" ]; then
      log_info "--- \\e[32mlogin\\e[0m using kubeconfig file provided by GitLab"
    else
      log_info "--- \\e[32mlogin\\e[0m using exploded kubeconfig parameters (env: \\e[33;1m${env}\\e[0m, url: \\e[33;1m${url}\\e[0m, namespace: \\e[33;1m${namespace}\\e[0m)"

      assert_defined "${url}" "Missing required Kubernetes URL. Provide a kubeconfig file or \$K8S_*_URL"
      assert_defined "${token}" "Missing required Kubernetes Token. Provide a kubeconfig file or \$K8S_*_TOKEN"

      export KUBECONFIG="$CI_PROJECT_DIR/.kubeconfig"
      touch "$KUBECONFIG"

      if [[ "$cacert" ]]
      then
        # Cluster config with CA cert
        mkdir -p "$CI_PROJECT_DIR/.kube/certs/k8s-cluster/"
        echo "$cacert" > "$CI_PROJECT_DIR/.kube/certs/k8s-cluster/k8s-ca.crt"
        kubectl config set-cluster k8s-cluster --certificate-authority="$CI_PROJECT_DIR/.kube/certs/k8s-cluster/k8s-ca.crt" --server="$url"
      else
        # Cluster config w/o CA cert
        kubectl config set-cluster k8s-cluster --server="$url"
      fi

      # Credentials config
      kubectl config set-credentials gitlab --token="$token"

      # Context config
      kubectl config set-context gitlab-k8s-cluster --cluster=k8s-cluster --user=gitlab --namespace="$namespace"
      kubectl config use-context gitlab-k8s-cluster
    fi

    # finally test connection and dump versions
    kubectl ${TRACE+-v=5} version
  }

  function deploy() {
    export env=$1
    # for backward compatibility
    export stage=$1
    export appname=$2
    # also export appname in SCREAMING_SNAKE_CASE format (may be useful with Kubernetes env variables)
    appname_ssc=$(to_ssc "$2")
    export appname_ssc
    # extract hostname from $CI_ENVIRONMENT_URL
    hostname=$(echo "$CI_ENVIRONMENT_URL" | awk -F[/:] '{print $4}')
    export hostname

    # for backward compatibility
    export K8S_APP_NAME=$appname
    export K8S_ENV=$env

    log_info "--- \\e[32mdeploy\\e[0m (env: \\e[33;1m${env}\\e[0m)"
    log_info "--- looking for Kubernetes scripts in directory: \\e[33;1m${K8S_SCRIPTS_DIR}\\e[0m"
    log_info "--- appname: \\e[33;1m${appname}\\e[0m"
    log_info "--- appname_ssc: \\e[33;1m${appname_ssc}\\e[0m"
    log_info "--- env: \\e[33;1m${env}\\e[0m"
    log_info "--- hostname: \\e[33;1m${hostname}\\e[0m"

    # maybe execute deploy script
    deployscript=$(ls -1 "$K8S_SCRIPTS_DIR/k8s-deploy-${env}.sh" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/k8s-deploy.sh" 2>/dev/null || echo "")
    if [[ -f "$deployscript" ]]
    then
      log_info "--- deploy script (\\e[33;1m${deployscript}\\e[0m) found: execute"
      sh "$deployscript"
    else
      log_info "--- no deploy script found: run template-based deployment"

      # maybe execute pre push script
      prescript="$K8S_SCRIPTS_DIR/k8s-pre-apply.sh"
      if [[ -f "$prescript" ]]; then
        log_info "--- \\e[32mpre-apply hook\\e[0m (\\e[33;1m${prescript}\\e[0m) found: execute"
        sh "$prescript"
      else
        log_info "--- \\e[32mpre-apply hook\\e[0m (\\e[33;1m${prescript}\\e[0m) not found: skip"
      fi

      # find deployment file
      deploymentfile=$(ls -1 "$K8S_SCRIPTS_DIR/deployment-${env}.yml" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/deployment.yml" 2>/dev/null || echo "")
      if [[ -z "$deploymentfile" ]]
      then
        log_error "--- deployment file not found"
        exit 1
      fi

      # replace variables (alternative for envsubst which is not present in image)
      awkenvsubst < "$deploymentfile" > generated-deployment.yml

      # apply/rollout deployment descriptor
      log_info "--- \\e[32mkubectl apply\\e[0m"
      kubectl ${TRACE+-v=5} apply -f ./generated-deployment.yml

      # maybe execute post apply script
      postscript="$K8S_SCRIPTS_DIR/k8s-post-apply.sh"
      if [[ -f "$postscript" ]]; then
        log_info "--- \\e[32mpost-apply hook\\e[0m (${postscript}) found: execute"
        sh "$postscript"
      else
        log_info "--- \\e[32mpost-apply hook\\e[0m (${postscript}) not found: skip"
      fi
    fi

    # persist environment url
    echo "$CI_ENVIRONMENT_URL" > environment_url.txt
    echo -e "environment_type=$env\\nenvironment_name=$appname\\nenvironment_url=$CI_ENVIRONMENT_URL" > kubernetes.env

    # maybe execute readiness check script
    readycheck="$K8S_SCRIPTS_DIR/k8s-readiness-check.sh"
    if [[ -f "$readycheck" ]]; then
      log_info "--- \\e[32mreadiness-check hook\\e[0m (\\e[33;1m${readycheck}\\e[0m) found: execute"
      sh "$readycheck"
    else
      log_info "--- \\e[32mreadiness-check hook\\e[0m (\\e[33;1m${readycheck}\\e[0m) not found: assume app is ready"
    fi
  }

  function rollback() {
    export env=$1
    # for backward compatibility
    export stage=$1
    export appname=$2
    # also export appname in SCREAMING_SNAKE_CASE format (may be useful with Kubernetes env variables)
    appname_ssc=$(to_ssc "$2")
    export appname_ssc
    # extract hostname from $CI_ENVIRONMENT_URL
    hostname=$(echo "$CI_ENVIRONMENT_URL" | awk -F[/:] '{print $4}')
    export hostname

    # for backward compatibility
    export K8S_APP_NAME=$appname
    export K8S_ENV=$env

    log_info "--- \\e[32mrollback\\e[0m (env: \\e[33;1m${env}\\e[0m)"
    log_info "--- looking for Kubernetes scripts in directory: \\e[33;1m${K8S_SCRIPTS_DIR}\\e[0m)"
    log_info "--- appname: \\e[33;1m${appname}\\e[0m"
    log_info "--- appname_ssc: \\e[33;1m${appname_ssc}\\e[0m"
    log_info "--- env: \\e[33;1m${env}\\e[0m"
    log_info "--- hostname: \\e[33;1m${hostname}\\e[0m"

    # maybe execute pre cleanup script
    prescript="$K8S_SCRIPTS_DIR/k8s-pre-rollback.sh"
    if [[ -f "$prescript" ]]; then
      log_info "--- \\e[32mpre-rollback hook\\e[0m (\\e[33;1m${prescript}\\e[0m) found: execute"
      sh "$prescript"
    else
      log_info "--- \\e[32mpre-rollback hook\\e[0m (\\e[33;1m${prescript}\\e[0m) not found: skip"
    fi

    # rollback app
    log_info "--- \\e[32mkubectl rollout undo\\e[0m"

    kubectl ${TRACE+-v=5} rollout undo deployment "$appname-$env"

    # maybe execute post cleanup script
    postscript="$K8S_SCRIPTS_DIR/k8s-post-rollback.sh"
    if [[ -f "$postscript" ]]; then
      log_info "--- \\e[32mpost-rollback hook\\e[0m (\\e[33;1m${postscript}\\e[0m) found: execute"
      sh "$postscript"
    else
      log_info "--- \\e[32mpost-rollback hook\\e[0m (\\e[33;1m${postscript}\\e[0m) not found: skip"
    fi
  }

  function cleanup() {
    export env=$1
    # for backward compatibility
    export stage=$1
    export appname=$2
    # also export appname in SCREAMING_SNAKE_CASE format (may be useful with Kubernetes env variables)
    appname_ssc=$(to_ssc "$2")
    export appname_ssc

    # for backward compatibility
    export K8S_APP_NAME=$appname
    export K8S_ENV=$env

    log_info "--- \\e[32mcleanup\\e[0m (env: ${env}, appname: ${appname})"
    log_info "--- appname: \\e[33;1m${appname}\\e[0m"
    log_info "--- appname_ssc: \\e[33;1m${appname_ssc}\\e[0m"
    log_info "--- env: \\e[33;1m${env}\\e[0m"

    # maybe execute cleanup script
    cleanupscript=$(ls -1 "$K8S_SCRIPTS_DIR/k8s-cleanup-${env}.sh" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/k8s-cleanup.sh" 2>/dev/null || echo "")
    if [[ -f "$cleanupscript" ]]
    then
      log_info "--- cleanup script (\\e[33;1m${cleanupscript}\\e[0m) found: execute"
      sh "$cleanupscript"
    else
      log_info "--- no cleanup script found: proceed with template-based delete"

      # maybe execute pre cleanup script
      prescript="$K8S_SCRIPTS_DIR/k8s-pre-cleanup.sh"
      if [[ -f "$prescript" ]]; then
        log_info "--- \\e[32mpre-cleanup hook\\e[0m (\\e[33;1m${prescript}\\e[0m) found: execute"
        sh "$prescript"
      else
        log_info "--- \\e[32mpre-cleanup hook\\e[0m (\\e[33;1m${prescript}\\e[0m) not found: skip"
      fi

      # find deployment file
      deploymentfile=$(ls -1 "$K8S_SCRIPTS_DIR/deployment-${env}.yml" 2>/dev/null || ls -1 "$K8S_SCRIPTS_DIR/deployment.yml" 2>/dev/null || echo "")
      if [[ -z "$deploymentfile" ]]
      then
        log_error "--- deployment file not found"
        exit 1
      fi

      # has to be valuated for envsubst
      export hostname=hostname

      # replace variables (alternative for envsubst which is not present in image)
      awkenvsubst < "$deploymentfile" > generated-deployment.yml

      # delete all objects from deployment file
      log_info "--- \\e[32mkubectl delete\\e[0m"
      kubectl ${TRACE+-v=5} delete -f ./generated-deployment.yml

      # maybe execute post cleanup script
      postscript="$K8S_SCRIPTS_DIR/k8s-post-cleanup.sh"
      if [[ -f "$postscript" ]]; then
        log_info "--- \\e[32mpost-cleanup hook\\e[0m (\\e[33;1m${postscript}\\e[0m) found: execute"
        sh "$postscript"
      else
        log_info "--- \\e[32mpost-cleanup hook\\e[0m (\\e[33;1m${postscript}\\e[0m) not found: skip"
      fi
    fi
  }

  function score() {
    deploymentfiles=""
    if [[ -f "$K8S_SCRIPTS_DIR/deployment-review.yml" ]]; then
      awkenvsubst < "$K8S_SCRIPTS_DIR/deployment-review.yml" > "$K8S_SCRIPTS_DIR/generated-deployment-review.yml"
      deploymentfiles="$K8S_SCRIPTS_DIR/generated-deployment-review.yml"
    fi
    if [[ -f "$K8S_SCRIPTS_DIR/deployment-integration.yml" ]]; then
      awkenvsubst < "$K8S_SCRIPTS_DIR/deployment-integration.yml" > "$K8S_SCRIPTS_DIR/generated-deployment-integration.yml"
      deploymentfiles="$deploymentfiles $K8S_SCRIPTS_DIR/generated-deployment-integration.yml"
    fi
    if [[ -f "$K8S_SCRIPTS_DIR/deployment-staging.yml" ]]; then
      awkenvsubst < "$K8S_SCRIPTS_DIR/deployment-staging.yml" > "$K8S_SCRIPTS_DIR/generated-deployment-staging.yml"
      deploymentfiles="$deploymentfiles $K8S_SCRIPTS_DIR/generated-deployment-staging.yml"
    fi
    if [[ -f "$K8S_SCRIPTS_DIR/deployment-production.yml" ]]; then
      awkenvsubst < "$K8S_SCRIPTS_DIR/deployment-production.yml" > "$K8S_SCRIPTS_DIR/generated-deployment-production.yml"
      deploymentfiles="$deploymentfiles $K8S_SCRIPTS_DIR/generated-deployment-production.yml"
    fi
    if [[ -f "$K8S_SCRIPTS_DIR/deployment.yml" ]]; then
      awkenvsubst < "$K8S_SCRIPTS_DIR/deployment.yml" > "$K8S_SCRIPTS_DIR/generated-deployment.yml"
      deploymentfiles="$deploymentfiles $K8S_SCRIPTS_DIR/generated-deployment.yml"
    fi

    # shellcheck disable=SC2086
    /usr/bin/kube-score score $K8S_SCORE_EXTRA_OPTS $deploymentfiles
  }

  function get_latest_template_version() {
    tag_json=$(wget -T 5 -q -O - "$CI_API_V4_URL/projects/to-be-continuous%2F$1/repository/tags?per_page=1" || echo "")
    echo "$tag_json" | sed -rn 's/^.*"name":"([^"]*)".*$/\1/p'
  }

  function check_for_update() {
    template="$1"
    actual="$2"
    latest=$(get_latest_template_version "$template")
    echo $latest
    echo $latest
    echo $latest
    if [[ -n "$latest" ]] && [[ "$latest" != "$actual" ]]
    then
      log_warn "\\e[1;93m=======================================================================================================\\e[0m"
      log_warn "\\e[93mThe template \\e[32m$template\\e[93m:\\e[33m$actual\\e[93m you're using is not up-to-date: consider upgrading to version \\e[32m$latest\\e[0m"
      log_warn "\\e[93m(set \$TEMPLATE_CHECK_UPDATE_DISABLED to disable this message)\\e[0m"
      log_warn "\\e[1;93m=======================================================================================================\\e[0m"
    fi
  }

  if [[ "$TEMPLATE_CHECK_UPDATE_DISABLED" != "true" ]]; then check_for_update kubernetes "2.0.2"; fi

  # export tool functions (might be used in after_script)
  export -f log_info log_warn log_error assert_defined rollback

  unscope_variables
  eval_all_secrets

  # ENDSCRIPT

.k8s-base:
  image:
    name: $K8S_KUBECTL_IMAGE
    entrypoint: [""]
  services:
    - name: "$CI_REGISTRY/to-be-continuous/tools/tracking:master"
      command: ["--service", "kubernetes", "2.0.2" ]
  before_script:
    - *k8s-scripts
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"

k8s-score:
  extends: .k8s-base
  stage: test
  image:
    name: $K8S_KUBE_SCORE_IMAGE
    entrypoint: [""]
  before_script:
    - *k8s-scripts
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
  script:
    - score
  rules:
    # exclude merge requests
    - if: $CI_MERGE_REQUEST_ID
      when: never
    # exclude when $K8S_SCORE_DISABLED is set
    - if: '$K8S_SCORE_DISABLED == "true"'
      when: never
    # else: allow failure
    - allow_failure: true

# Deploy job prototype
# Can be extended to define a concrete environment
#
# @arg ENV_TYPE       : environment type
# @arg ENV_APP_NAME   : env-specific application name
# @arg ENV_APP_SUFFIX : env-specific application suffix
# @arg ENV_URL        : env-specific Kubernetes API url
# @arg ENV_SPACE      : env-specific Kubernetes namespace
# @arg ENV_TOKEN      : env-specific Kubernetes API token
# @arg ENV_CA_CERT    : env-specific Kubernetes CA certificate
# @arg ENV_KUBE_CONFIG: env-specific Kubeconfig
.k8s-deploy:
  extends: .k8s-base
  stage: deploy
  variables:
    ENV_APP_SUFFIX: "-$CI_ENVIRONMENT_SLUG"
  before_script:
    - *k8s-scripts
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - assert_defined "${ENV_SPACE:-$K8S_SPACE}" 'Missing required env $ENV_SPACE or $K8S_SPACE'
    - login "$ENV_TYPE" "${ENV_URL:-$K8S_URL}" "${ENV_CA_CERT:-$K8S_CA_CERT}" "${ENV_TOKEN:-$K8S_TOKEN}" "${ENV_SPACE:-$K8S_SPACE}" "${ENV_KUBE_CONFIG:-${K8S_DEFAULT_KUBE_CONFIG}}"
  script:
    - deploy "$ENV_TYPE" ${ENV_APP_NAME:-${K8S_BASE_APP_NAME}${ENV_APP_SUFFIX}}
  artifacts:
    name: "$ENV_TYPE env url for $CI_PROJECT_NAME on $CI_COMMIT_REF_SLUG"
    when: always
    paths:
      - generated-deployment.yml
      - environment_url.txt
    reports:
      dotenv: kubernetes.env

# Cleanup job prototype
# Can be extended for each deletable environment
#
# @arg ENV_TYPE       : environment type
# @arg ENV_APP_NAME   : env-specific application name
# @arg ENV_APP_SUFFIX : env-specific application suffix
# @arg ENV_URL        : env-specific Kubernetes API url
# @arg ENV_SPACE      : env-specific Kubernetes namespace
# @arg ENV_TOKEN      : env-specific Kubernetes API token
# @arg ENV_CA_CERT    : env-specific Kubernetes CA certificate
# @arg ENV_KUBE_CONFIG: env-specific Kubeconfig
.k8s-cleanup:
  extends: .k8s-base
  stage: deploy
  # force no dependencies
  dependencies: []
  before_script:
    - *k8s-scripts
    - install_ca_certs "${CUSTOM_CA_CERTS:-$DEFAULT_CA_CERTS}"
    - assert_defined "${ENV_SPACE:-$K8S_SPACE}" 'Missing required env $ENV_SPACE or $K8S_SPACE'
    - login "$ENV_TYPE" "${ENV_URL:-$K8S_URL}" "${ENV_CA_CERT:-$K8S_CA_CERT}" "${ENV_TOKEN:-$K8S_TOKEN}" "${ENV_SPACE:-$K8S_SPACE}" "${ENV_KUBE_CONFIG:-${K8S_DEFAULT_KUBE_CONFIG}}"
  script:
    - cleanup "$ENV_TYPE" ${ENV_APP_NAME:-${K8S_BASE_APP_NAME}${ENV_APP_SUFFIX}}
  environment:
    action: stop

k8s-review:
  extends: .k8s-deploy
  variables:
    ENV_TYPE: review
    ENV_APP_NAME: "$K8S_REVIEW_APP_NAME"
    ENV_URL: "$K8S_REVIEW_URL"
    ENV_SPACE: "$K8S_REVIEW_SPACE"
    ENV_TOKEN: "$K8S_REVIEW_TOKEN"
    ENV_CA_CERT: "$K8S_REVIEW_CA_CERT"
    ENV_KUBE_CONFIG: "$K8S_REVIEW_KUBE_CONFIG"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    url: "${K8S_REVIEW_ENVIRONMENT_SCHEME}://${CI_PROJECT_NAME}-${CI_ENVIRONMENT_SLUG}.${K8S_REVIEW_ENVIRONMENT_DOMAIN}"
    on_stop: k8s-cleanup-review
  resource_group: review/$CI_COMMIT_REF_NAME
  rules:
    # exclude merge requests
    - if: $CI_MERGE_REQUEST_ID
      when: never
    # exclude tags
    - if: $CI_COMMIT_TAG
      when: never
    # only on non-production, non-integration branches, with $K8S_REVIEW_SPACE set
    - if: '$K8S_REVIEW_SPACE && $CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'

# stop review env (automatically triggered once branches are deleted)
k8s-cleanup-review:
  extends: .k8s-cleanup
  variables:
    ENV_TYPE: review
    ENV_APP_NAME: "$K8S_REVIEW_APP_NAME"
    ENV_URL: "$K8S_REVIEW_URL"
    ENV_SPACE: "$K8S_REVIEW_SPACE"
    ENV_TOKEN: "$K8S_REVIEW_TOKEN"
    ENV_CA_CERT: "$K8S_REVIEW_CA_CERT"
    ENV_KUBE_CONFIG: "$K8S_REVIEW_KUBE_CONFIG"
  environment:
    name: review/$CI_COMMIT_REF_NAME
    action: stop
  resource_group: review/$CI_COMMIT_REF_NAME
  rules:
    # exclude merge requests
    - if: $CI_MERGE_REQUEST_ID
      when: never
    # exclude tags
    - if: $CI_COMMIT_TAG
      when: never
    # only on non-production, non-integration branches, with $K8S_REVIEW_SPACE set
    - if: '$K8S_REVIEW_SPACE && $CI_COMMIT_REF_NAME !~ $PROD_REF && $CI_COMMIT_REF_NAME !~ $INTEG_REF'
      when: manual
      allow_failure: true

k8s-integration:
  extends: .k8s-deploy
  variables:
    ENV_TYPE: integration
    ENV_APP_NAME: "$K8S_INTEG_APP_NAME"
    ENV_URL: "$K8S_INTEG_URL"
    ENV_SPACE: "$K8S_INTEG_SPACE"
    ENV_TOKEN: "$K8S_INTEG_TOKEN"
    ENV_CA_CERT: "$K8S_INTEG_CA_CERT"
    ENV_KUBE_CONFIG: "$K8S_INTEG_KUBE_CONFIG"
  environment:
    name: integration
    url: "${K8S_INTEG_ENVIRONMENT_URL}"
  resource_group: integration
  rules:
    # exclude merge requests
    - if: $CI_MERGE_REQUEST_ID
      when: never
    # only on integration branch(es), with $K8S_INTEG_SPACE set
    - if: '$K8S_INTEG_SPACE && $CI_COMMIT_REF_NAME =~ $INTEG_REF'

###############################
# Staging deploys are disabled by default since
# continuous deployment to production is enabled by default
# If you prefer to automatically deploy to staging and
# only manually promote to production, enable this job by setting
# $K8S_STAGING_SPACE.
k8s-staging:
  extends: .k8s-deploy
  variables:
    ENV_TYPE: staging
    ENV_APP_NAME: "$K8S_STAGING_APP_NAME"
    ENV_URL: "$K8S_STAGING_URL"
    ENV_SPACE: "$K8S_STAGING_SPACE"
    ENV_TOKEN: "$K8S_STAGING_TOKEN"
    ENV_CA_CERT: "$K8S_STAGING_CA_CERT"
    ENV_KUBE_CONFIG: "$K8S_STAGING_KUBE_CONFIG"
  environment:
    name: staging
    url: "${K8S_STAGING_ENVIRONMENT_URL}"
  resource_group: staging
  rules:
    # exclude merge requests
    - if: $CI_MERGE_REQUEST_ID
      when: never
    # only on production branch(es), with $K8S_STAGING_SPACE set
    - if: '$K8S_STAGING_SPACE && $CI_COMMIT_REF_NAME =~ $PROD_REF'

# deploy to production if on branch master and variable K8S_PROD_SPACE defined and AUTODEPLOY_TO_PROD is set
k8s-production:
  extends: .k8s-deploy
  stage: production
  variables:
    ENV_TYPE: production
    ENV_APP_SUFFIX: "" # no suffix for prod
    ENV_APP_NAME: "$K8S_PROD_APP_NAME"
    ENV_URL: "$K8S_PROD_URL"
    ENV_SPACE: "$K8S_PROD_SPACE"
    ENV_TOKEN: "$K8S_PROD_TOKEN"
    ENV_CA_CERT: "$K8S_PROD_CA_CERT"
    ENV_KUBE_CONFIG: "$K8S_PROD_KUBE_CONFIG"
  environment:
    name: production
    url: "${K8S_PROD_ENVIRONMENT_URL}"
  resource_group: production
  rules:
    # exclude merge requests
    - if: $CI_MERGE_REQUEST_ID
      when: never
    # exclude non-production branches
    - if: '$CI_COMMIT_REF_NAME !~ $PROD_REF'
      when: never
    # exclude if $K8S_PROD_SPACE not set
    - if: '$K8S_PROD_SPACE == null || $K8S_PROD_SPACE == ""'
      when: never
    # if $AUTODEPLOY_TO_PROD: auto
    - if: $AUTODEPLOY_TO_PROD
    # else if PUBLISH_ON_PROD enabled: auto (because the publish job was blocking)
    - if: '$PUBLISH_ON_PROD == "true" || $PUBLISH_ON_PROD == "yes"'
    # else: manual, blocking
    - if: $K8S_PROD_SPACE # useless test, just to prevent GitLab warning
      when: manual
